# Get group details for the required roles.
# You may also manage the groups themselves in the same module.
data "gitlab_group" "backend_engineers" {
  full_path = "roles/backend-engineers"
}

data "gitlab_group" "frontend_engineers" {
  full_path = "roles/frontend-engineers"
}

# Manage users
# Here we manage them manually: one user per resource.
# However, in production is probably gets more dynamic than that:
# the users may come from a Terraform variable and resources
# are dynamically created using `for_each`, for example.
resource "gitlab_user" "peter_parker" {
  name     = "Peter Parker"
  username = "peterparker"
  email    = "peter.parker@marvel.example.com"
}

resource "gitlab_group_membership" "peter_parker_frontend" {
  user_id      = gitlab_user.peter_parker.id
  group_id     = data.gitlab_group.frontend_engineers.id
  access_level = "developer"
}

resource "gitlab_user" "jessica_drew" {
  name     = "Jessica Drew"
  username = "jessicadrew"
  email    = "jessica.drew@marvel.example.com"
}

resource "gitlab_group_membership" "jessica_drew_backend" {
  user_id      = gitlab_user.jessica_drew.id
  group_id     = data.gitlab_group.backend_engineers.id
  access_level = "maintainer"
}