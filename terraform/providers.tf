terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 15.8"
    }
  }
}

provider "gitlab" {
  base_url = "https://gdk.test:3443"
}